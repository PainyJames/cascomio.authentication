package com.cascomio.authentication

import akka.http.scaladsl.Http
import routes.{LoginRoute, TokenRoute}
import akka.http.scaladsl.server.Directives._

object Startup extends App with AuthenticationModule {
  val routes = LoginRoute.getRoutes() ~ TokenRoute.getRoutes()
  val interface = config.getString("http.interface")
  val port = config.getInt("http.port")
  Http() bindAndHandle(routes, interface, port) map { binding =>
    println(s"Running on ${binding.localAddress}")
  }
}

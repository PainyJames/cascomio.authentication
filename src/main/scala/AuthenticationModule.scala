package com.cascomio.authentication

import com.cascomio.authentication.actors.DefaultEntryPointActor
import akka.actor.{ActorRef, ActorSystem, Props}
import akka.routing.RoundRobinPool
import akka.stream.ActorMaterializer
import com.typesafe.config.{Config, ConfigFactory}

import scala.concurrent.ExecutionContext


trait AuthenticationModule {
  implicit val system : ActorSystem = ActorSystem()
  implicit val materializer : ActorMaterializer = ActorMaterializer()
  implicit val executionContext : ExecutionContext = system.dispatcher
  implicit val config : Config = ConfigFactory.load()
  implicit val httpClient : HttpClient = new DefaultHttpClient
  val props = DefaultEntryPointActor.props().withRouter(RoundRobinPool(5));
  implicit val actor : ActorRef = system.actorOf(props, "entry-point")
}

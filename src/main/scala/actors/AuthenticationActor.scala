package com.cascomio.authentication.actors

import akka.actor.{Actor, ActorLogging, ActorRef, ActorSystem, Props}
import akka.routing.RoundRobinPool
import akka.util.Timeout

import scala.concurrent.duration._
import akka.pattern.{ask, pipe}
import akka.stream.Materializer
import com.typesafe.config.Config

import scala.concurrent.ExecutionContext

object AuthenticationActor {
  case class LoginPage(site: String)
  def props()(implicit config: Config, ec: ExecutionContext, as: ActorSystem, m: Materializer) = Props(new AuthenticationActor)
}

final class AuthenticationActor
   (googleAuthenticationActorRef: Option[ActorRef] = None)
   (implicit config: Config, ec: ExecutionContext, as: ActorSystem, m: Materializer)
  extends Actor with ActorLogging {
  import AuthenticationActor._
  private lazy val googleAuthenticationActors = googleAuthenticationActorRef.getOrElse(context.actorOf(RoundRobinPool(5).props(GoogleAuthenticationActor.props), "google-auth"))

  implicit val askTimeout: Timeout = 5.seconds
  def receive = {
    case LoginPage("Facebook") =>
      sender ! "Facebbook OK"
    case request @ LoginPage(x) if (x.toLowerCase() == config.getString("auth.google.state")) =>
      val result = googleAuthenticationActors ? request
      pipe(result) to sender
    case LoginPage("Twitter") =>
      sender ! "Twitter OK"
    case LoginPage("Linkedin") =>
      sender ! "Linkedin OK"
    case _ => sender ! "No support for that kind of login"
  }
}


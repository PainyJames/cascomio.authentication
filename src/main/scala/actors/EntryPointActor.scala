package com.cascomio.authentication.actors

import akka.actor.{Actor, ActorLogging, ActorRef, ActorSystem, Props}
import akka.pattern.{ask, pipe}
import akka.routing.RoundRobinPool
import akka.stream.Materializer
import akka.util.Timeout
import com.cascomio.authentication.HttpClient
import com.cascomio.authentication.actors.AuthTokenActor.LoginRedirect
import com.cascomio.authentication.actors.AuthenticationActor.LoginPage
import com.typesafe.config.Config

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._

abstract class EntryPointActor(authenticationActorRef: Option[ActorRef] = None, authTokenActorsRef: Option[ActorRef] = None)(implicit httpClient: HttpClient, config: Config, ec: ExecutionContext, as: ActorSystem, m: Materializer) extends Actor
final class DefaultEntryPointActor(authenticationActorRef: Option[ActorRef] = None, authTokenActorsRef: Option[ActorRef] = None)
     (implicit httpClient: HttpClient, config: Config, ec: ExecutionContext, as: ActorSystem, m: Materializer)
  extends EntryPointActor with ActorLogging {
  private lazy val authenticationActors = authenticationActorRef.getOrElse(context.actorOf(RoundRobinPool(5).props(AuthenticationActor.props), "authentication"))
  private lazy val authTokenActors = authTokenActorsRef.getOrElse(context.actorOf(RoundRobinPool(5).props(AuthTokenActor.props), "auth-token"))

  implicit val askTimeout: Timeout = 10.seconds

  def receive =
  {
    case loginPage @ LoginPage(_) => {
      val result = authenticationActors ? loginPage
      pipe(result) to sender
    }
    case loginRedirect @ LoginRedirect(_) => {
      val result = authTokenActors ? loginRedirect
      pipe(result) to sender
    }
    case x @ _ =>
      sender ! s"$x is not a valid request"
  }
}

object DefaultEntryPointActor {
  def props()(implicit httpClient: HttpClient, config: Config, ec: ExecutionContext, as: ActorSystem, m: Materializer) = Props(new DefaultEntryPointActor)
}
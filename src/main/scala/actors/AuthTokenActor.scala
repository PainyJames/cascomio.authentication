package com.cascomio.authentication.actors

import akka.actor.{Actor, ActorLogging, ActorRef, ActorSystem, Props}
import akka.pattern.{ask, pipe}
import akka.routing.RoundRobinPool
import akka.stream.Materializer
import akka.util.Timeout
import com.cascomio.authentication.HttpClient
import com.typesafe.config.Config

import scala.concurrent.duration._
import scala.concurrent.ExecutionContext

object AuthTokenActor {
  case class LoginRedirect(parameters: Map[String, String])
  case class LoginToken(accessToken: String, refreshToken: String)
  def props()(implicit httpClient: HttpClient, config: Config, ec: ExecutionContext, as: ActorSystem, m: Materializer) = Props(new AuthTokenActor)
}

final class AuthTokenActor(googleAuthTokenActorRef: Option[ActorRef] = None)
                          (implicit httpClient: HttpClient,
                                    config: Config,
                                    ec: ExecutionContext,
                                    as: ActorSystem,
                                    m: Materializer)
  extends Actor with ActorLogging {
  import AuthTokenActor._
  private lazy val googleAuthTokenActors = googleAuthTokenActorRef.getOrElse(context.actorOf(RoundRobinPool(5).props(GoogleAuthTokenActor.props), "google-auth-token"))

  implicit val askTimeout: Timeout = 5.seconds
  def receive = {
    case request @ LoginRedirect(t) if t.get("state") == Some(config.getString("auth.google.state")) =>
      pipe(googleAuthTokenActors ? request) to sender
    case _ =>
      sender ! "Nothing matches"
  }
}

package com.cascomio.authentication.actors

import akka.actor.{Actor, ActorLogging, ActorSystem, Props}
import akka.pattern.pipe
import akka.http.scaladsl.model.HttpMethods
import akka.stream.Materializer
import com.cascomio.authentication.HttpClient
import com.cascomio.authentication.actors.AuthTokenActor.LoginRedirect
import com.typesafe.config.Config

import scala.concurrent.ExecutionContext

final class GoogleAuthTokenActor()(implicit httpClient: HttpClient, config: Config, ec: ExecutionContext, as: ActorSystem, m: Materializer) extends Actor with ActorLogging {
  def receive = {
    case request @ LoginRedirect(_) => {
      val googleConfig = config.getConfig("auth.google")
      val uri = googleConfig.getString("token_url")
      val parameterList = List("grant_type", "client_id", "client_secret", "redirect_uri", "state")
      val parameters : Map[String, String] = (for(
        p <- parameterList
      ) yield p -> googleConfig.getString(p)).toMap
      val codeParameter = "code" -> request.parameters.getOrElse("code", "")
      val response = httpClient.execute(uri, httpMethod = HttpMethods.POST, parameters = parameters + codeParameter)
      pipe(response) to sender
    }
    case _ => sender ! "bad request"
  }
}
object GoogleAuthTokenActor {
  def props()(implicit httpClient: HttpClient, config: Config, ec: ExecutionContext, as: ActorSystem, m: Materializer) = Props(new GoogleAuthTokenActor)
}

package com.cascomio.authentication.actors

import akka.actor.{Actor, ActorLogging, ActorSystem, Props}
import akka.http.scaladsl.model.Uri
import akka.http.scaladsl.model.Uri.Query
import akka.stream.Materializer
import com.cascomio.authentication.actors.AuthenticationActor.LoginPage
import com.typesafe.config.Config

import scala.concurrent.ExecutionContext

final class GoogleAuthenticationActor()(implicit config: Config, ec: ExecutionContext, as: ActorSystem, m: Materializer) extends Actor with ActorLogging {
  def receive = {
    case LoginPage(x) if (x.toLowerCase() == config.getString("auth.google.state")) => {
      val googleConfig = config.getConfig("auth.google")
      val uri = googleConfig.getString("auth_url")
      val parameterList = List("redirect_uri", "access_type", "api_key", "client_id", "scope", "response_type", "prompt", "state")
      val parameters : Map[String, String] = (for(
        p <- parameterList
      ) yield p -> googleConfig.getString(p)).toMap
      val googleUrl = Uri(uri).withQuery(Query(parameters)).toString()
      sender ! googleUrl
    }
    case _ => sender ! "bad request"
  }
}
object GoogleAuthenticationActor {
  def props()(implicit config: Config, ec: ExecutionContext, as: ActorSystem, m: Materializer) = Props(new GoogleAuthenticationActor)
}

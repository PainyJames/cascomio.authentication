package com.cascomio.authentication

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.Uri.Query
import akka.http.scaladsl.model._
import akka.stream.Materializer

import scala.concurrent.{ExecutionContext, Future}
import scala.concurrent.duration._

trait HttpClient { def execute(uri: String, payload: Option[RequestEntity] = None, httpMethod: HttpMethod = HttpMethods.GET, parameters: Map[String, String]) (implicit ec : ExecutionContext, as: ActorSystem, m: Materializer) : Future[String] }
class DefaultHttpClient extends HttpClient {
  def execute(uri: String, payload: Option[RequestEntity] = None, httpMethod: HttpMethod = HttpMethods.GET, parameters: Map[String, String] = Map[String, String]())
             (implicit ec : ExecutionContext, as: ActorSystem, m: Materializer) : Future[String] = {
    val uriWithQuery = Uri(uri).withQuery(Query(parameters))
    val response = for {
      response <- Http().singleRequest(HttpRequest(method = httpMethod, uri = uriWithQuery))
      json <- response.entity.toStrict(5 seconds).map(_.data.decodeString("UTF-8"))
    } yield json
    response
  }
}

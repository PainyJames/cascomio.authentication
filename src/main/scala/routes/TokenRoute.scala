package routes

import akka.actor.ActorRef
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.coding.Deflate
import akka.http.scaladsl.model.HttpEntity
import akka.http.scaladsl.server.Route
import akka.pattern.ask
import akka.util.Timeout
import com.cascomio.authentication.actors.AuthTokenActor.LoginRedirect

import scala.concurrent.duration._

object TokenRoute {
  def getRoutes()(implicit actor: ActorRef) : Route = {
    val routes = path("token") {
      implicit val askTimeout: Timeout = 20.seconds
      get {
          parameterMap { params => {
            encodeResponseWith(Deflate) {
              onSuccess((actor ? new LoginRedirect(params)).mapTo[String]) { result =>
                complete(HttpEntity(result))
              }
            }
          }
        }
      }
    }
    routes
  }
}

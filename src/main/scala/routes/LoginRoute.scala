package routes

import akka.actor.ActorRef
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.coding.Gzip
import akka.http.scaladsl.model.HttpEntity
import akka.http.scaladsl.server.Route
import akka.pattern.ask
import akka.util.Timeout
import com.cascomio.authentication.actors.AuthenticationActor.LoginPage

import scala.concurrent.duration._

object LoginRoute {
  def getRoutes()(implicit actor: ActorRef) : Route = {
    val routes = path("login") {
      implicit val askTimeout: Timeout = 20.seconds
      get {
        parameters('site).as(LoginPage) { loginPage => {
            encodeResponseWith(Gzip) {
              onSuccess((actor ? loginPage).mapTo[String]) { result =>
                complete(HttpEntity(result))
              }
            }
          }
        }
      }
    }
    routes
  }
}

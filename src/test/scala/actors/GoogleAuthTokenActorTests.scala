package com.cascomio.authentication.tests.actors

import akka.actor.ActorSystem
import akka.http.scaladsl.model.{HttpMethod, RequestEntity}
import akka.stream.Materializer
import akka.testkit.TestProbe
import com.cascomio.authentication.HttpClient
import com.cascomio.authentication.actors.AuthTokenActor.LoginRedirect
import com.cascomio.authentication.actors.GoogleAuthTokenActor
import com.cascomio.authentication.tests.BaseAuthenticationTestsWithImplicits
import com.typesafe.config.{Config, ConfigFactory}

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}

final class GoogleAuthTokenActorTests extends BaseAuthenticationTestsWithImplicits("GoogleAuthTokenActorTests") {

  implicit override val config : Config = ConfigFactory.load()
  val jsonResponse = "{\"token\": \"Test\""

  override def afterAll {
    shutdown()
  }

  class TestHttpClient extends HttpClient {
    override def execute(uri: String, payload: Option[RequestEntity], httpMethod: HttpMethod, parameters: Map[String, String])(implicit ec: ExecutionContext, as: ActorSystem, m: Materializer): Future[String] = Future(jsonResponse)
  }
  object TestHttpClient extends TestHttpClient {}

  "A Google Auth Token actor" should {
    "Return request the access token to google" in {
      implicit val httpClient : HttpClient = TestHttpClient
      val googleAuthTokenActor = system.actorOf(GoogleAuthTokenActor.props())
      val message = LoginRedirect(Map("code" -> "12345"))
      val probe = TestProbe()

      probe.send(googleAuthTokenActor, message)

      val response = probe.receiveOne(300 millis).asInstanceOf[String]
      assert(response == jsonResponse)
    }
  }
}

package com.cascomio.authentication.tests.actors

import akka.actor.Props
import akka.testkit.TestProbe
import com.cascomio.authentication.actors.AuthenticationActor.LoginPage
import com.cascomio.authentication.actors.GoogleAuthenticationActor
import com.cascomio.authentication.tests.BaseAuthenticationTestsWithImplicits
import com.typesafe.config.ConfigFactory

import scala.concurrent.duration._

final class GoogleAuthenticationActorTests extends BaseAuthenticationTestsWithImplicits("GoogleAuthenticationActorTests") {

  implicit override val config = ConfigFactory.load()

  override def afterAll {
    shutdown()
  }

  "A Google Authentication actor" should {
    "Return google login page url" in {
      val entryPointActor = system.actorOf(Props(new GoogleAuthenticationActor))
      val message = LoginPage("Google")
      val probe = TestProbe()

      probe.send(entryPointActor, message)

      val response = probe.receiveOne(300 millis).asInstanceOf[String]

      assert(response.contains("google.com"))
    }
  }
}

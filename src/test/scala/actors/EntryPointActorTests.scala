package com.cascomio.authentication.tests.actors

import akka.actor.Props
import akka.testkit.{TestActorRef, TestProbe}
import com.cascomio.authentication.actors.AuthTokenActor.LoginRedirect
import com.cascomio.authentication.actors.AuthenticationActor.LoginPage
import com.cascomio.authentication.actors.{AuthTokenActor, AuthenticationActor, DefaultEntryPointActor}
import com.cascomio.authentication.tests.BaseAuthenticationTestsWithImplicits

import scala.concurrent.duration._

final class EntryPointActorTests extends BaseAuthenticationTestsWithImplicits("EntryPointTests") {

  val authenticationActors = TestActorRef(new AuthenticationActor)
  val authTokenActors = TestActorRef(new AuthTokenActor)

  override def afterAll {
    shutdown()
  }

  "An EntryPoint actor" should {
    "Forward login page request messages to autheticationActors" in {
      within(300 millis) {
        val probe = TestProbe()
        probe watch authenticationActors
        val msg = LoginPage("Test")
        val entryPointActor = system.actorOf(Props(new DefaultEntryPointActor(Some(probe.ref), Some(authTokenActors))))

        entryPointActor ! msg

        probe.expectMsg(msg)
      }
    }
    "Forward token redirect response request messages to authTokenActors" in {
      within(300 millis) {
        val probe = TestProbe()
        probe watch authTokenActors
        val msg = LoginRedirect(Map("oompa" -> "lumpa"))
        val entryPointActor = system.actorOf(Props(new DefaultEntryPointActor(Some(authenticationActors), Some(probe.ref))))

        entryPointActor ! msg

        probe.expectMsg(msg)
      }
    }
  }

}

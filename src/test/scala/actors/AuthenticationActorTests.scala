package com.cascomio.authentication.tests.actors

import akka.actor.Props
import akka.testkit.{TestActorRef, TestProbe}
import com.cascomio.authentication.actors.AuthenticationActor.LoginPage
import com.cascomio.authentication.actors.{AuthenticationActor, GoogleAuthenticationActor}
import com.cascomio.authentication.tests.BaseAuthenticationTestsWithImplicits
import com.typesafe.config.ConfigFactory

import scala.concurrent.duration._

final class AuthenticationActorTests extends BaseAuthenticationTestsWithImplicits("AuthenticationActorTests") {

  implicit override val config = ConfigFactory.load()
  val googleAuthenticationActors = TestActorRef(new GoogleAuthenticationActor)

  override def afterAll {
    shutdown()
  }

  "An Authentication actor" should {
    "Forward Google requests to its actor" in {
      within(300 millis) {
        val probe = TestProbe()
        probe watch googleAuthenticationActors
        val message = LoginPage("Google")
        val authenticationActor = system.actorOf(Props(new AuthenticationActor(Some(probe.ref))))

        authenticationActor ! message

        probe.expectMsg(message)
      }
    }
  }
}

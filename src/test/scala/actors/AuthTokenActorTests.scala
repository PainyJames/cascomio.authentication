package com.cascomio.authentication.tests.actors

import akka.actor.Props
import akka.testkit.{TestActorRef, TestProbe}
import com.cascomio.authentication.actors.AuthTokenActor.LoginRedirect
import com.cascomio.authentication.actors.{AuthTokenActor, GoogleAuthTokenActor}
import com.cascomio.authentication.tests.BaseAuthenticationTestsWithImplicits
import com.typesafe.config.ConfigFactory

import scala.concurrent.duration._

final class AuthTokenActorTests extends BaseAuthenticationTestsWithImplicits("AuthTokenActorTests")  {

  implicit override val config = ConfigFactory.load()
  val googleAuthTokenActors = TestActorRef(new GoogleAuthTokenActor)

  override def afterAll {
    shutdown()
  }

  "An Authentication actor" should {
    "Forward Google requests to its actor" in {
      within(300 millis) {
        val probe = TestProbe()
        probe watch googleAuthTokenActors
        val message = LoginRedirect(Map("state" -> "google"))
        val authTokenActor = system.actorOf(Props(new AuthTokenActor(Some(probe.ref))))

        authTokenActor ! message

        probe.expectMsg(message)
      }
    }
  }
}

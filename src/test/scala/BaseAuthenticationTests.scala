package com.cascomio.authentication.tests

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.testkit.{DefaultTimeout, ImplicitSender, TestKit}
import com.cascomio.authentication.HttpClient
import com.typesafe.config.Config
import org.scalamock.scalatest.MockFactory
import org.scalatest.{BeforeAndAfterAll, FlatSpecLike, Matchers, WordSpecLike}

abstract class BaseAuthenticationTests(name: String) extends TestKit(ActorSystem(name))
with WordSpecLike
with BeforeAndAfterAll
with Matchers
with DefaultTimeout
with MockFactory
with ImplicitSender {
}

abstract class BaseAuthenticationTestsWithImplicits(name: String) extends BaseAuthenticationTests(name) {
  implicit val config = mock[Config]
  implicit val asystem = ActorSystem()
  implicit val materializer = ActorMaterializer()
  implicit val executionContext = system.dispatcher
  implicit val httpClient = mock[HttpClient]
}

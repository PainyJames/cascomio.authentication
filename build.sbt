name := "cascomio.authentication"

version := "1.0"

scalaVersion := "2.12.1"
val AkkaVersion = "2.4.17"
val AkkaHttpVersion = "10.0.3"
val ScalaTestVersion = "3.0.1"
val ScalaMockVersion = "3.5.0"

resolvers ++= Seq("Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/",
  "Java.net Maven2 Repository" at "http://download.java.net/maven/2/",
  Resolver.bintrayRepo("hseeberger", "maven"))

libraryDependencies ++= {
  Seq(
    "com.typesafe.akka" %% "akka-slf4j"      % AkkaVersion,
    "com.typesafe.akka" %% "akka-http" % AkkaHttpVersion,
    "com.typesafe.akka" %% "akka-http-spray-json" % AkkaHttpVersion,
    "com.typesafe.akka" %% "akka-testkit" % AkkaVersion,
    "org.scalactic" %% "scalactic" % ScalaTestVersion,
    "org.scalatest" %% "scalatest" % ScalaTestVersion % "test",
    "org.scalamock" %% "scalamock-scalatest-support" % ScalaMockVersion % Test
  )
}

mainClass in (Compile, run) := Some("com.cascomio.authentication.Startup")

mainClass in (Compile, packageBin) := Some("com.cascomio.authentication.Startup")
